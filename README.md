# SimpleMind MedSAM
MedSAM repo made compatible with SimpleMind. Please support and [cite](#reference) the [official repository](https://github.com/bowang-lab/MedSAM) if you use this!!! .

## Installation
Assuming you have an environment or docker running with SimpleMind already ([see SimpleMind documentation](https://gitlab.com/sm-ai-team/simplemind)):

```bash
git clone https://gitlab.com/gaby97/simplemind-medsam.git
cd simplemind-medsam
pip install -e .
```

## Usage
Download the MedSAM [model checkpoint](https://drive.google.com/drive/folders/1ETWmi4AiniJeWOt6HAsYgTjYv_fkgzoN?usp=drive_link). You can place it wherever your little heart desires. Just make sure whatever drive you saved it on is mounted on your environment.

### Calling MedSAM from the Knowledge Graph yaml

```yaml
  left_kidney_medsam:
    runner: local
    entrypoint: "external-medsam"
    attributes:
      image: Promise(image as image from load_image)
      bounding_box: Promise(left_kidney_bb as mask from left_kidney_bb)
      medsam_checkpoint: "./medsam_weights/medsam_vit_b.pth"
      output_name: medsam
      output_type: left_mask_for_ensemble
      output_dir: *output_dir 
      file_based: *file_based
```

Entrypoint will be ```external-medsam```.

#### Attributes/Inputs:
- ```image```: Receives as promise an image or list of images to run inference on.
- ```bounding_box```: Receives as a promise a binary mask that forms a bounding box around the target of interest. A regular, non-box, mask can be used as well, but I recommend using the ```mask_processing-bounding_box``` or ```mask_processing-spatial_offset``` agents if you want to manipulate the size of the bounding box using mm.
- ```medsam_checkpoint```: The path to where you stored your MedSAM weights.
- ```output_name``` and ```output_type``` will default to ```mask``` and ```mask_compressed_numpy```, respectively, if not specified.

## Reference

```
@article{MedSAM,
  title={Segment Anything in Medical Images},
  author={Ma, Jun and He, Yuting and Li, Feifei and Han, Lin and You, Chenyu and Wang, Bo},
  journal={Nature Communications},
  volume={15},
  pages={1--9},
  year={2024}
}
```
